# Running the example application

The example application reads input from stdin and writes to stdout.

    $ printf '1 1 1\na\nb' | cargo run --example alienlanguage --release -q
    Case #1: 0

    $ printf '1 2 1\na\nb\n(ab)' | cargo run --example alienlanguage --release -q
    Case #1: 2

    $ printf '2 2 2\naa\nab\n(ab)(ab)\n(ab)(ac)' | cargo run --example alienlanguage --release -q
    Case #1: 2
    Case #2: 1

    $ cargo run --example alienlanguage --release -q < input/sample.txt
    Case #1: 2
    Case #2: 1
    Case #3: 3
    Case #4: 0

Where the `cargo run` command is building if necessary, and then simply running...

    $ target/release/examples/alienlanguage < input/sample.txt
    Case #1: 2
    Case #2: 1
    Case #3: 3
    Case #4: 0


# Running the web service

The web service listens on 8080 and accepts plain text.

    $ cargo run --release -q &
    Listening { socket: V4(0.0.0.0:8080) }
    
    $ curl -X POST localhost:8080 --data-binary @input/sample.txt
    Case #1: 2
    Case #2: 1
    Case #3: 3
    Case #4: 0
