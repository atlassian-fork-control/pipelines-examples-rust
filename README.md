# An example of using Bitbucket Pipelines for a Rust project

[Bitbucket](https://bitbucket.org/product/features) is the Git solution for professional teams.
[Bitbucket Pipelines](https://confluence.atlassian.com/display/BITBUCKET/Get+started+with+Bitbucket+Pipelines)
allows you to build, test and deploy from Bitbucket.

[Rust](https://www.rust-lang.org/) is a systems programming language that runs blazingly fast,
prevents segfaults, and guarantees thread safety. If you are new to Rust check out
[Setting up a Rust project](docs/setting-up-a-rust-project.md).

The project centers around matching patterns in a noisy signal against a dictionary of known words
from an [alien language](docs/alien-language.md). To demonstrate the capabilities of Bitbucket
Pipelines the project includes the following aspects:

* a library in the [src/](src/lib.rs) directory that is
  [documented](https://doc.rust-lang.org/book/documentation.html) and contains the following types
  of tests:
    * [unit tests](https://doc.rust-lang.org/book/testing.html#the-tests-module)
    * [documentation tests](https://doc.rust-lang.org/book/testing.html#documentation-tests)
    * [benchmark tests](https://doc.rust-lang.org/book/benchmark-tests.html)
* [integration tests](https://doc.rust-lang.org/book/testing.html#the-tests-directory) in the
  [tests/](tests/alienlanguage.rs) directory
* an example in the [examples/](examples/alienlanguage.rs) directory


Bitbucket Pipelines is used to

* Build the project
* Format the source code
* Run unit tests
* Test the project library's documentation
* Run integration tests
* Run benchmark tests
* Publish the project's API documentation
* Upload the project to the crates.io registry
* Deploy the web service to AWS (TODO)
* Cache build dependencies to speed up future Pipelines


The instructions for the build are contained in [bitbucket-pipelines.yml](bitbucket-pipelines.yml).
The build uses [buildpack-deps-rust](https://hub.docker.com/r/atlassianlabs/buildpack-deps-rust/) as
the base image. The image includes a nightly version of the Rust toolchain along with the
[rustfmt](https://github.com/rust-lang-nursery/rustfmt/blob/master/README.md) utility.

Here is a glimpse at the
[Bitbucket Pipelines environment variables](https://bitbucket.org/repo/aMLbbB/images/3769013299-pipelines_env.png)
that are setup for the project. You can see the status of all the builds on
[this repositories Pipelines page](https://bitbucket.org/atlassian/pipelines-examples-rust/addon/pipelines/home).

Here are details of each step.


## Update Rust toolchain

The `rustup` utility is used to update to the latest nightly version of the Rust toolchain.


## Setup git with read/write access

This is required for pushing up code formatting changes and pushing the generated API docs to the
Bitbucket-hosted static website.


## Build project

A quick check to make sure the code compiles.


## Format the source code

If formatting changes are made, they are automatically commited and pushed back up. The build is
killed, and another build starts up.


## Run unit tests

Unit tests are contained in a tests module in the same file as the library code they are testing.


## Run documentation tests

Documentation examples are tested.


## Run integration tests

Integration tests in the test directory are run.


## Run benchmark tests

Benchmarks are contained in a tests module in the same file as the library code they are testing.


## Build the project's documentation

`cargo doc` is used to generate API docs. The docs are published to a
[static website](http://alienlanguage.bitbucket.org/)
[hosted by Bitbucket](https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket+Cloud).


## Upload to crates.io

crates.io is a public repository for Rust libraries. It is used to find dependencies listed in
Cargo.toml. Here is this projects [crates.io page](https://crates.io/crates/alienlanguage).

TODO: bump version, git tag


## Deploy to AWS

TODO: [deploy the web service to AWS](https://bitbucket.org/shivaman/bb_codedeploy_helloworld/src).
